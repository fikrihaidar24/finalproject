<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePostinganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_postingan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('posting_id');
            $table->unsignedBigInteger('users_id');
            $table->foreign('posting_id')->references('id')->on('posting')->onDelete('cascade');
            $table->foreign('users_id')->references('id')->on('users');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_postingan');
    }
}
