<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::Create([
            'name' => 'Fikri Haidar',
            'email' => 'fikrihaidar24@gmail.com',
            'password' => bcrypt('admin123')
        ]);
        $user2 = User::Create([
            'name' => 'Rohadi Wiguna',
            'email' => 'rohadinyoman@gmail.com',
            'password' => bcrypt('admin123')
        ]);
        $user3 = User::Create([
            'name' => 'Rayi An Nafi',
            'email' => 'annafiakbar@gmail.com',
            'password' => bcrypt('admin123')
        ]);
    }
}
