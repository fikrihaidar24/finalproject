@extends('layouts/master');
@section('content')
    
<div class="card">
  <h5 class="card-title">Edit Komentar</h5>
  <div class="card-body">
    <ul class="we-comet">

      <li class="post-comment">
        <div class="comet-avatar">
          <img src="{{asset('assets/images/resources/comet-1.jpg')}}" alt="">
        </div>
        <div class="post-comt-box">
          <form method="post" action="/komentar/{{$komen->id}}">
            @csrf
            @method('PUT')
            <textarea placeholder="Post your comment" id="komen" name="komen">{{$komen->isi}}</textarea>
            <input type="submit" value="Komen" class="btn-sm btn-primary float-right">
          </form>	
        </div>
      </li>
    </ul>
  </div>
</div>

@endsection

