@extends('layouts/master')

@section('content')
<div class="central-meta new-pst">
  <div class="new-postbox">
    <figure>
      <img src="images/resources/admin2.jpg" alt="">
    </figure>
    <div class="newpst-input">
      
      <form method="POST" action="/Post/{{$post->id}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <textarea rows="2" placeholder="write something" id="isi" name="isi">{{$post->isi}}</textarea>
        <div class="attachments">
          <ul>
            <li>
              <i class="fa fa-image"></i>
              <label class="fileContainer">
                <input type="file" name="foto">
              </label>
            </li>
            <li>
              <button type="submit">Post</button>
            </li>
          </ul>
        </div>
      </form>
    </div>
  </div>
</div><!-- add post new box -->
<div class="loadMore">
<div class="central-meta item">
  <div class="user-post">
    {{-- POST --}}
    {{-- @include('timeline.post') --}}
    {{-- ENDPOST --}}
    <div class="coment-area">
      {{-- KOMENTAR --}}
    
      {{-- ENDKOMENTAR --}}
    </div>
  </div>
</div>
</div>
@endsection
