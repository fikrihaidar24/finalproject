@foreach ($post as $d)
<div class="friend-info">
  <figure>
    <img src="{{asset('assets/images/resources/friend-avatar10.jpg')}}" alt=""}>
  </figure>
  <div class="friend-name">
    <ins><a href="" title="">{{$d->user->name}}</a></ins>
    <span>published: {{$d->created_at}}</span>
  </div>
  @if($d->user_id === Auth::id())
  {{-- <a class="btn-sm btn-primary float-right" data-toggle="modal"     data-target="#exampleModal" href="/pertanyaan/{{$d->id}}/edit/">
      <i class="fas fa-edit"></i>
          Edit
  </a> --}}
  <a href="/Post/{{$d->id}}/edit" class="btn btn-md btn-success float-right">Edit</a>
  <form action="/Post/{{$d->id}}" method="POST">
    @csrf
    @method('DELETE')
    <div class="submit-btns">
      <input type="submit" value="Hapus" class="btn-sm btn-danger float-right">
    </div>
   
  </form>

@endif
  <div class="post-meta">
    <img src="{{url('/post_image/'.$d->foto)}}" alt="">
    <div class="we-video-info">
      <ul>
        <li>
          <span class="comment" data-toggle="tooltip" title="Comments">
            <i class="fa fa-comments-o"></i>
            <ins>{{$d->komentar->count()}}</ins>
          </span>
        </li>
        <li>
        
          {{-- <a href="/post/unlike/{{$d->id}}">
          <span class="dislike" data-toggle="tooltip" title="dislike">
            <i class="ti-heart-broken"></i>
            
          </span>
          <ins>Jumlah Like {{$d->likes->count()}}</ins>
        </a>
          @else --}}
        
          <a href="/post/like/{{$d->id}}">
          <span class="like" data-toggle="tooltip" title="like">
            <i class="ti-heart"></i>
            <ins>{{$d->likes->count()}}</ins>
          </span>
        </a>
        
        </li>
      </ul>
    </div>
    <div class="description">
      
      <p>
        {{$d->isi}}
      </p>
    </div>
  </div>
</div>
@foreach ($d->komentar as $k)
    

<ul class="we-comet">
  <li>
    <div class="comet-avatar">
      <img src="{{asset('assets/images/resources/comet-1.jpg')}}" alt="">
    </div>
    <div class="we-comment">
      @if($k->users_id === Auth::id())
      <a href="/komentar/{{$k->id}}/edit" class="btn btn-md btn-success float-right">Edit</a>
    <form action="/posts/{{$k->id}}/komentar" method="POST">
      @csrf
      @method('DELETE')
      <input type="submit" value="Hapus" class="btn-sm btn-danger float-right" style="height:25px; width:60px">
    </form>
    {{-- <button action="/posts/{{$k->id}}/edit" class="btn btn-primary btn-sm my-1 mr-1">Edit</button> --}}
    @endif
      <div class="coment-head">
        <h5><a href="time-line.html" title="">{{$k->user->name}}</a></h5>
        <span>{{$k->created_at}}</span>
        <a class="we-reply" href="#" title="Reply"><i class="fa fa-reply"></i></a>
      </div>
      <p>{{$k->isi}}
       
      </p>
     
      <a href="/komen/like/{{$k->id}}">
        <span class="like" data-toggle="tooltip" title="like">
          <i class="ti-heart"></i>
          <ins>{{$k->likes->count()}}</ins>
        </span>
      </a>  
    
     
    </div>
  </li>
  @endforeach
  <li class="post-comment" style=" list-style-type: none;">
    <div class="comet-avatar">
      <img src="images/resources/comet-1.jpg" alt="">
    </div>
    <div class="post-comt-box">
      <form method="post" action="/posts/{{$d->id}}/komentar">
        @csrf
        <textarea placeholder="Post your comment" id="komen" name="komen"></textarea>
        <input type="submit" value="Komen" class="btn-sm btn-primary float-right">
      </form>	
    </div>
  </li>
</ul>

@endforeach
