@extends('layouts/master_profile')

@section('user')
@foreach ($profiles as $p)


<section>
  <div class="feature-photo">
    <figure><img src="{{url('/post_image/'.$p->cover_photo)}}" alt=""></figure>
    <div class="add-btn">

      <span>{{$following->count()}} Following</span>
      <span>{{$follower->count()}} Followers</span>

      <a href="#" title="" data-ripple="">Add Friend</a>
    </div>
    <div class="container-fluid">
      <div class="row merged">
        <div class="col-lg-2 col-sm-3">
          <div class="user-avatar">
            <figure>
              <img src="{{url('/post_image/'.$p->display_photo)}}" alt="">
            </figure>
          </div>
        </div>
        <div class="col-lg-10 col-sm-9">
          <div class="timeline-info">
            <ul>
              <li class="admin-name">
                <h5>{{Auth::user()->name}}</h5>
                <span>Group Admin</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- top area -->
<div class="central-meta">
	<div class="about">
		<div class="personal">
			<h5 class="f-title"><i class="ti-info-alt"></i> Personal Info</h5>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</p>
		</div>
		<div class="d-flex flex-row mt-2">
			<ul class="nav nav-tabs nav-tabs--vertical nav-tabs--left" >
				<li class="nav-item">
					<a href="#basic" class="nav-link active" data-toggle="tab" >Basic info</a>
				</li>
				<li class="nav-item">
					<a href="#work" class="nav-link" data-toggle="tab" >work and education</a>
				</li>
				<li class="nav-item">
					<a href="#interest" class="nav-link" data-toggle="tab"  >interests</a>
				</li>
				<li class="nav-item">
					<a href="#lang" class="nav-link" data-toggle="tab" >languages</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade show active" id="basic" >
					<ul class="basics">
						<li><i class="ti-user"></i>{{$p->first_name}}  {{$p->last_name}}</li>
						<li><i class="ti-map-alt"></i>live in {{$p->alamat}}</li>
						<li><i class="ti-email"></i>{{$p->email}}</li>
						<li><i class="ti-world"></i>www.yoursite.com</li>
					</ul>
				</div>
				<div class="tab-pane fade" id="work" role="tabpanel">
					<div>
						<a href="#" title="">Envato</a>
						<p>work as autohr in envato themeforest from 2013</p>
						<ul class="education">
							<li><i class="ti-facebook"></i> BSCS from Oxford University</li>
							<li><i class="ti-twitter"></i> MSCS from Harvard Unversity</li>
						</ul>
					</div>
				</div>
				<div class="tab-pane fade" id="interest" role="tabpanel">
					<ul class="basics">
						<li>Footbal</li>
						<li>internet</li>
						<li>photography</li>
					</ul>
				</div>
				<div class="tab-pane fade" id="lang" role="tabpanel">
					<ul class="basics">
						<li>english</li>
						<li>french</li>
						<li>spanish</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endforeach
@endsection
