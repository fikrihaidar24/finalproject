@extends('layouts/master_profile')

@section('user')

<div class="col-lg-12">
    <div class="central-meta">
        <div class="editing-info">
            <h5 class="f-title"><i class="ti-info-alt"></i> Create Basic Information</h5>

            <form method="POST" action="/profile" enctype="multipart/form-data">
                @csrf
                <div class="form-group half">
                  <input type="text" id="first_name" name="first_name"  required="required"/>
                  <label class="control-label" for="first_name">First Name</label><i class="mtrl-select"></i>
                </div>
                <div class="form-group half">
                  <input type="text" id="last_name" name="last_name" value="{{ old('last_name', '') }}" required="required"/>
                  <label class="control-label" for="last_name">Last Name</label><i class="mtrl-select"></i>
                </div>
                <div class="form-group">
                  <input type="text" id="email" name="email" value="{{ old('email', '') }}" required="required"/>
                  <label class="control-label" for="email"><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="21644c40484d61">email</a></label><i class="mtrl-select"></i>
                </div>
                {{-- <div class="form-group">
                  <input type="text"  required="required"/>
                  <label class="control-label" for="input">Phone No.</label><i class="mtrl-select"></i>
                </div> --}}
                {{-- <div class="dob">
                    <div class="form-group">
                        <label for="tgl">Tanggal Lahir</label>
                        <input type="date" name="tgl" id="tgl" class="form-control" required>
                    </div>
                </div> --}}
                <div class="form-group">
                    <label for="gender">Choose Gender</label>
                    <select name="gender" id="gender" class="form-control">
                        <option value="L">Laki - Laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                  <input type="text" id="alamat" name="alamat" value="{{ old('alamat', '') }}" required="required"/>
                  <label class="control-label" for="alamat">Alamat</label><i class="mtrl-select"></i>
                </div>
                <div class="form-group">
                  <input type="file" name="display" class="custom-file-input" id="customFile">
                  <label class="custom-file-label" for="customFile">Masukan Display Photo</label>
                </div>
                <div class="form-group">
                  <input type="file" name="cover" class="custom-file-input" id="customFile">
                  <label class="custom-file-label" for="customFile">Masukan Cover Photo</label>
                </div>
                
                
                {{-- <div class="form-group">
                    <label for="cover_photo">Cover Photo</label>
                    <input type="file" name="cover_photo" id="cover_photo" class="form-control">
                </div> --}}
                {{-- <div class="form-group">
                  <textarea rows="4" id="textarea"></textarea>
                  <label class="control-label" for="textarea">About Me</label><i class="mtrl-select"></i>
                </div> --}}
                <div class="submit-btns">
                    <button type="button" class="mtr-btn"><span>Cancel</span></button>
                    <button type="submit" class="mtr-btn"><span>Submit</span></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
