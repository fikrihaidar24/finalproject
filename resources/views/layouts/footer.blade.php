<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="widget">
						<div class="foot-logo">
							<div class="logo">
								<a href="index.html" title=""><img src="images/logo.png" alt=""></a>
							</div>	
							<p>
								Final Project Kelompok 7
								<ul>
									<li>Fikri Haidar Nugraha</li>
									<li>Rohadi Wiguna</li>
									<li>Rayi An nafi</li>	
								</ul> 
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4">
					<div class="widget">
						<div class="widget-title"><h4>follow</h4></div>
						<ul class="list-style">
							<li><i class="fa fa-git"></i> <a href="https://gitlab.com/fikrihaidar24/finalproject.git" title="">Git</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-4">
					<div class="widget">
						<div class="widget-title"><h4>Clone This Social Media</h4></div>
						<ul class="colla-apps">
							<li><a href="https://gitlab.com/fikrihaidar24/finalproject.git" title=""><i class="fa fa-git"></i>Gitlab</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- footer -->