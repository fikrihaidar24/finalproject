<section>
  <div class="feature-photo">
    <figure><img src="{{asset('assets/images/resources/timeline-1.jpg')}}" alt=""></figure>
    <div class="add-btn">
    
      <span>{{$following->count()}} Following</span>
      <span>{{$follower->count()}} Followers</span>
     
      
    </div>
    <div class="container-fluid">
      <div class="row merged">
        <div class="col-lg-2 col-sm-3">
          <div class="user-avatar">
            <figure>
              <img src="{{url('/post_image/'.$d->foto)}}" alt="">
            </figure>
          </div>
        </div>
        <div class="col-lg-10 col-sm-9">
          <div class="timeline-info">
            <ul>
              <li class="admin-name">
                <h5>{{Auth::user()->name}}</h5>
                <span>Group Admin</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- top area -->