<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from wpkixx.com/html/winku/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 12 Jan 2020 16:10:42 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title>Winku Social Network Toolkit</title>
    <link rel="icon" href="{{asset('assets/images/fav.png')}}" type="image/png" sizes="16x16">

    <link rel="stylesheet" href="{{asset('assets/css/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/color.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

</head>
<body>
<!--<div class="se-pre-con"></div>-->
<div class="theme-layout">
  <div class="postoverlay"></div>



  {{-- TOPBAR --}}
  @include('layouts.topbar')
	{{-- ENDTOPBAR --}}

		<section class="tabs-section">
			<div class="tabs-section-nav tabs-section-nav-left">
				@yield('tabheader')
			</div><!--.tabs-section-nav-->
			<div class="tab-content no-styled profile-tabs">
				@yield('profilecontent')
			</div>
		</section>


	<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row merged20" id="page-contents">

							<div class="col-lg-12">
								{{-- Content --}}
									@yield('content')
								{{-- endcontent --}}
							</div><!-- centerl meta -->

							{{-- SIDEBAR --}}
							{{-- @include('layouts.sidebar') --}}
							{{-- ENDSIDEBAR --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


  @include('layouts.footer');

	<div class="bottombar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<span class="copyright">© Winku 2018. All rights reserved.</span>
					<i><img src="images/credit-cards.png" alt=""></i>
				</div>
			</div>
		</div>
	</div>
</div>


  <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
  <script src="{{asset('assets/js/main.min.js')}}"></script>
	<script src="{{asset('assets/js/script.js')}}"></script>
	<script src="{{asset('assets/js/map-init.js')}}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8c55_YHLvDHGACkQscgbGLtLRdxBDCfI"></script>

	@stack('scripts')
	@include('sweetalert::alert')

</body>	

<!-- Mirrored from wpkixx.com/html/winku/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 12 Jan 2020 16:11:41 GMT -->
</html>
