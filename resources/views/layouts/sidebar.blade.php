<div class="col-lg-3">
  <aside class="sidebar static">
    <div class="widget">
      <h4 class="widget-title">Your page</h4>	
      <div class="your-page">
        <figure>
          <a href="#" title=""><img src="{{asset('assets/images/resources/friend-avatar9.jpg')}}" alt=""></a>
        </figure>
        <div class="page-meta">
          <a href="#" title="" class="underline">My page</a>
        </div>
        <div class="page-likes">
          <ul class="nav nav-tabs likes-btn">
            <li class="nav-item"><a class="active" href="#link1" data-toggle="tab">Following</a></li>
             <li class="nav-item"><a class="" href="#link2" data-toggle="tab">Follower</a></li>
          </ul>
          <!-- Tab panes -->
          
          <div class="tab-content">
            <div class="tab-pane active fade show " id="link1" >
            <span><i class="ti-heart"></i>{{$following->count()}}</span>
              <a href="#" title="weekly-likes">35 new likes this week</a>
              <div class="users-thumb-list">
              <a href="#" title="Anderw" data-toggle="tooltip">
                <img src="{{asset('assets/images/resources/userlist-1.jpg')}}" alt="">  
              </a>
              </div>
            </div>
            <div class="tab-pane active fade show " id="link2" >
              <span><i class="ti-heart"></i>{{$follower->count()}}</span>
                <a href="#" title="weekly-likes">35 new likes this week</a>
                <div class="users-thumb-list">
                <a href="#" title="Anderw" data-toggle="tooltip">
                  <img src="{{asset('assets/images/resources/userlist-1.jpg')}}" alt="">  
                </a>
                </div>
              </div>
          </div>
          
          
        </div>
      </div>
    </div><!-- page like widget -->
  </aside>
</div><!-- sidebar -->