<div class="topbar stick">
  <div class="logo">
    <a title="" href=""><img src="{{asset('assets/images/logo.png')}}" alt=""></a>
  </div>

  <div class="top-area">
    <ul class="main-menu">
        <a href="/home" title="">TimeLine</a>
    </ul>
    <ul class="setting-area">
      <li>
        <a href="#" title="Home" data-ripple=""><i class="ti-search"></i></a>
        <div class="searched">
          <form method="get" action="{{route('users.cari')}}" class="form-search">
            <input type="text" id="cari" name="cari" placeholder="Search Friend">
            <button data-ripple><i class="ti-search"></i></button>
          </form>
        </div>
      </li>
      <li>
        <a href="#" title="Notification" data-ripple="">
          <i class="ti-bell"></i><span></span>
        </a>
        <div class="dropdowns">

          <ul class="drops-menu">
            <li>
              <a href="/notification" title="">
                <div class="mesg-meta">
                  <span class="text-center">Lihat Notofikasi</span>

                </div>
              </a>
              <span class="tag">New</span>
            </li>
          </ul>
        </div>
      </li>
    </ul>
    <div class="user-img">
      <span>{{ Auth::user()->name }}</span>
      <img src="{{asset('assets/images/resources/admin.jpg')}}" alt="">
      <span class="status f-online"></span>
      <div class="user-setting">
        <a href="/profile" title=""><i class="ti-user"></i> view profile</a>
        <a href="/profile/create" title=""><i class="ti-plus"></i> create profile</a>
        <a href="/profile/{{ Auth::user()->id }}/edit" title=""><i class="ti-pencil-alt"></i>edit profile</a>
        <a href="{{ route('logout') }}" onclick= "event.preventDefault(); document.getElementById('logout-form').submit();">
       <i class="ti-power-off"></i>log out</a>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
      </div>
    </div>

  </div>
</div><!-- topbar -->
