@extends('layouts.landing')

@section('content')

<div class="log-reg-area sign">
    <h2 class="log-title">Login</h2>
        <p>
            Don’t use Winku Yet? <a href="#" title="">Take the tour</a> or <a href="#" title="">Join now</a>
        </p>
        <form method="POST" action="{{ route('login') }}">
            @csrf
        <div class="form-group">
            <input type="text" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required="required"/>
          <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
          @error('email')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
            @error('password')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="checkbox">
          <label>
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
          </label>
        </div>
        <div class="submit-btns">
            <button class="mtr-btn signin" type="submit"><span>Login</span></button>
            {{-- <button class="mtr-btn signup" type="submit"><span>Register</span></button> --}}
            <a class="btn btn-primary" href="{{ route('register') }}">{{ ('Register') }}</a>

            @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password ?') }}
            </a>
    @endif
        </div>
    </form>
</div>
@endsection
@push('script')
<script src="{{ asset('assets/js/script.js') }}"></script>
<script>
    $('button.signup').on("click", function(){
		$('.login-reg-bg').addClass('show');
		return false;
	  });
</script>
@endpush
