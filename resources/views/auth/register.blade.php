@extends('layouts.landing')

@section('content')

<div class="log-reg-area sign">
    <h2 class="log-title">Register</h2>
        <p>
            Don’t use Winku Yet? <a href="#" title="">Take the tour</a> or <a href="#" title="">Join now</a>
        </p>
        <form method="POST" action="{{ route('register') }}">
            @csrf
        <div class="form-group">
            <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required="required"/>
          <label class="control-label" for="input">Name</label><i class="mtrl-select"></i>
          @error('name')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
        <div class="form-group">
            <input type="text" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required="required"/>
          <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
          @error('email')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            <label class="control-label" for="password">{{ 'Password' }}</label><i class="mtrl-select"></i>
            @error('password')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">
            <label class="control-label" for="input">{{ __('Confirm Password') }}</label><i class="mtrl-select"></i>
        </div>
        <div class="checkbox">
          <label>
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
          </label>
        </div>
        <div class="submit-btns">
            <button type="submit" class="btn btn-primary">
                {{ __('Register') }}
            </button>
        </div>
    </form>
</div>
@endsection
