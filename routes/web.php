<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@timeline')->name('home');

Route::resource('Post', 'PostingController');



Route::resource('profile', 'ProfileController');

// Route::get('profile/edit/{id}', 'ProfileController');

//follow User
Route::get('/user/cari', 'UserController@cari')->name('users.cari');
Route::get('/following/{id}', 'FollowerController@follow');
Route::get('/notification', 'FollowerController@notification');
Route::get('/accept/{id}', 'FollowerController@accept')->name('accept');

//likepost
Route::get('/post/like/{id}', 'PostingController@like');
Route::get('/post/unlike/{id}', 'PostingController@unlike');

//Likekomen
Route::get('komen/like/{id}','KomentarController@like');

//Komentar
Route::post('/posts/{id}/komentar', 'KomentarController@store')->name('komentar.store');
Route::get('/komentar/{id}/edit', 'KomentarController@edit');
Route::put('/komentar/{id}', 'KomentarController@update');
Route::delete('/posts/{id}/komentar', 'KomentarController@destroy')->name('komentar.destroy');
