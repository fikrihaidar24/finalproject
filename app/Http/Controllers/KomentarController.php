<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posting;
Use App\Komentar;
use App\Likekomen;
use Auth;

class KomentarController extends Controller
{
    
    public function store(Request $request,$id)
    {
        
        $post = Posting::find($id);
        $post_id = $post->id;
       
        Komentar::create([
            'posting_id' => $post_id,
            'users_id'=> Auth::id(),
            'isi'     => $request->get('komen')
        ]);

        return back();
    }
    public function like($id)
    {
        Likekomen::create([
            'komentar_id' => $id,
            'users_id' => Auth::id()
        ]);

        

        return back();
    }

    public function edit($id)
    {
        $komen = Komentar::where('id',$id)->first();
        // dd($post);
        return view ('timeline.editkomen',compact('komen'));
    }

    public function update(Request $request,$id)
    {
        
       
        $komen = Komentar::where('id',$id)->update([
            'users_id'=> Auth::id(),
            'isi'     => $request->get('komen')
        ]);
        return redirect('home');

    }

    public function destroy($id)
    {
         Komentar::destroy($id);
         return redirect('home');

    }

    // public function edit($id)
    // {
    //     Komentar::edit($id);
    //     return view('timeline.editkomen', compact('post'));
    // }
}
