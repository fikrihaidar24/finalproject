<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Profile;
use App\User;
use App\Follower;
use DB;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $profiles = Profile::where('profiles.user_id','=',Auth::id())->get();

        $following = Follower::where('follower.user_id', '=', Auth::user()->id)
            ->join('users', 'users.id', '=', 'follower.follow_id')
            ->where('status', 1)
            ->get();
        $follower = Follower::where('follower.follow_id', '=', Auth::user()->id)
            ->join('users', 'users.id', '=', 'follower.user_id')
            ->where('status', 1)
            ->get();
        return view('profile.index', compact('profiles', 'following', 'follower'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $following = Follower::where('follower.user_id', '=', Auth::user()->id)
            ->join('users', 'users.id', '=', 'follower.follow_id')
            ->where('status', 1)
            ->get();
        $follower = Follower::where('follower.follow_id', '=', Auth::user()->id)
            ->join('users', 'users.id', '=', 'follower.user_id')
            ->where('status', 1)
            ->get();

        return view('profile.create', compact('following', 'follower'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $profile = new Profile;

        $display = request('display');
        $extention = $display->getClientOriginalExtension();
        $name = $display->getClientOriginalName();
        $size = $display->getSize();
        $mime = $display->getMimeType();
        $tujuan_upload = 'post_image';
        $display->move($tujuan_upload, $display->getClientOriginalName());

        $cover = request('cover');
        $extensi = $cover->getClientOriginalExtension();
        $nama = $cover->getClientOriginalName();
        $ukuran = $cover->getSize();
        $type = $cover->getMimeType();
        $tujuan = 'post_image';
        $cover->move($tujuan, $cover->getClientOriginalName());

        $profile::create([
            'user_id' => Auth::id(),
            'first_name'    =>  $request->get('first_name'),
            'last_name'     =>  $request->get('last_name'),
            'email'         =>  $request->get('email'),
            'gender'        =>  $request->get('gender'),
            'alamat'        =>  $request->get('alamat'),
            'display_photo' =>  $name,
            'cover_photo'   =>  $nama

        ]);

        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profiles = Profile::find($id);
        return view('/profile', compact('profiles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $profiles = Profile::latest()->limit(1)->get();
        $profiles = Profile::where('profiles.user_id', $id)->get();
        return view('profile.edit', compact('profiles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());


        $display = request('display');
        $extention = $display->getClientOriginalExtension();
        $name = $display->getClientOriginalName();
        $size = $display->getSize();
        $mime = $display->getMimeType();
        $tujuan_upload = 'post_image';
        $display->move($tujuan_upload, $display->getClientOriginalName());

        $cover = request('cover');
        $extensi = $cover->getClientOriginalExtension();
        $nama = $cover->getClientOriginalName();
        $ukuran = $cover->getSize();
        $type = $cover->getMimeType();
        $tujuan = 'post_image';
        $cover->move($tujuan, $cover->getClientOriginalName());

        $profile = Profile::where('id', $id)->update([
            'user_id' => Auth::id(),
            'first_name'    =>  $request->get('first_name'),
            'last_name'     =>  $request->get('last_name'),
            'email'         =>  $request->get('email'),
            'gender'        =>  $request->get('gender'),
            'alamat'        =>  $request->get('alamat'),
            'display_photo' =>  $name,
            'cover_photo'   =>  $nama

        ]);

        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
