<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Follower;
use Auth;

class FollowerController extends Controller
{
    public function follow($id){
        $follow = new Follower;
        $follow->user_id = Auth::user()->id;
        $follow->follow_id = $id;
        $follow->save();

        
        return back();
    }

    public function notification(){
        $notif = Follower::where('follower.follow_id','=',Auth::user()->id)
                ->join('users','users.id','=','follower.user_id')
                ->where('status',0)
                ->get();
        return view('timeline.notification',compact('notif'));
    }

    public function accept($id){
         Follower::where('follow_id',Auth::user()->id)
                  ->where('user_id',$id)
                  ->update(['status' => 1]);
        return back();
    }
    public function reject($id){
        Follower::where('follow_id',Auth::user()->id)
                 ->where('user_id',$id)
                 ->delete();
       return back();
   }

//    public function following(){
//     $following = Follower::where('follower.user_id','=',Auth::user()->id)
//             ->join('users','users.id','=','follower.follow_id')
//             ->where('status',1)
//             ->get();
//     return view('timeline.timeline',compact('following'));
// }
}
