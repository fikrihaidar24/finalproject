<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posting;
use App\User;
use App\Komentar;
use App\Follower;
use App\Profile;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('profile/index');
    }
    public function timeline()
    {
        $post = Posting::orderBy('id','desc')->get();
        $following = Follower::where('follower.user_id','=',Auth::user()->id)
        ->join('users','users.id','=','follower.follow_id')
        ->where('status',1)
        ->get();
        $follower = Follower::where('follower.follow_id','=',Auth::user()->id)
        ->join('users','users.id','=','follower.user_id')
        ->where('status',1)
        ->get();
        $profiles = Profile::where('profiles.id','=',Auth::user()->id)->first();
        return view('timeline/timeline',compact('post','following','follower','profiles'));
    }
    public function coba()
    {
        return view('profile/profile');
    }
}
