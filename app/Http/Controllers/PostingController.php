<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posting;
use Auth;
use App\User;
use App\Likepost;
use Session;
use RealRashid\SweetAlert\Facades\Alert;


class PostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if (request('foto')) {
            $post = new Posting;

            $foto = request('foto');
            $extention = $foto->getClientOriginalExtension();
            $name = $foto->getClientOriginalName();
            $size = $foto->getSize();
            $mime = $foto->getMimeType();
            $tujuan_upload = 'post_image';
            $foto->move($tujuan_upload, $foto->getClientOriginalName());

            $post::create([
                'user_id' => Auth::id(),
                'isi'     => $request->get('isi'),
                'foto'    => $name
            ]);
        } else {
            $post = new Posting;
            $post::create([
                'user_id' => Auth::id(),
                'isi'     => $request->get('isi'),

            ]);
        }
        Alert::success('Sukses', 'Postingan Berhasil Di Tambahkan');

        return redirect('home');
    }

    public function like($id)
    {
        Likepost::create([
            'posting_id' => $id,
            'users_id' => Auth::id()
        ]);

        
        Alert::success('Sukses', 'Berhasil Menyukai Postingan');
        return back();
    }

    public function unlike($id)
    {
        $like = Likepost::where('posting_id', $id)
            ->where('users_id', Auth::id())->first();
        $like->delete();
        return back();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Posting::where('id',$id)->first();
        // dd($post);
        return view ('timeline.editpost',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {if (request('foto')) {
        

        $foto = request('foto');
        $extention = $foto->getClientOriginalExtension();
        $name = $foto->getClientOriginalName();
        $size = $foto->getSize();
        $mime = $foto->getMimeType();
        $tujuan_upload = 'post_image';
        $foto->move($tujuan_upload, $foto->getClientOriginalName());

        $posting = Posting::where('id',$id)->update([
            'user_id' => Auth::id(),
            'isi'     => $request->get('isi'),
            'foto'    => $name
        ]);
    } else {
        $posting = Posting::where('id',$id)->update([
            'user_id' => Auth::id(),
            'isi'     => $request->get('isi'),

        ]);
    }
       
    Alert::success('Sukses', 'Berhasil Mengupdate Postingan');
        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Posting::destroy($id);
        Alert::success('Sukses', 'Berhasil Menghapus Postingan');
        return redirect('home');
    }
}
