<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likekomen extends Model
{
    protected $table = 'like_dislike_komentar';
    protected $fillable = [
        'komentar_id','users_id'
    ];
    
    public function user(){

    	return $this->belongsTo('App\User','users_id','id');
    }
}
