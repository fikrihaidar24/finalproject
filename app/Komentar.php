<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Komentar extends Model
{
    protected $table = "komentar";
    protected $fillable = [
     'isi','posting_id','users_id'   
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function likes(){
        return $this->hasMany('App\Likekomen','komentar_id','id');
    }
    public function komen_is_liked_by_user(){

        $id = Auth::id();

        $likers = array(); 

        foreach($this->likes as $like){
            array_push($likers, $like->users_id);
        }

    if(in_array($id,$likers))
    {
        return true;
    }
    // else{
    //     return false;
    // }

    }
}
