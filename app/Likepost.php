<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likepost extends Model
{
    protected $table = 'like_dislike_postingan';
    protected $fillable = [
        'users_id','posting_id'
    ];

    public function user(){

    	return $this->belongsTo('App\User','users_id','id');
    }
}
