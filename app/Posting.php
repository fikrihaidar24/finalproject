<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Posting extends Model
{
    protected $table = 'posting';
    protected $fillable = [
        'isi', 'user_id', 'foto'
    ];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function likes(){
        return $this->hasMany('App\Likepost','posting_id','id');
    }

    public function komentar(){
        return $this->hasMany('App\Komentar','posting_id','id');
    }

    public function is_liked_by_user(){

        $id = Auth::id();

        $likers = array(); 

        foreach($this->likes as $like){
            array_push($likers, $like->users_id);
        }

    if(in_array($id,$likers))
    {
        return true;
    }
    // else{
    //     return false;
    // }

    }
}
